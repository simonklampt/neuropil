#
# neuropil development enviroment Dockerfile
#
#

FROM ubuntu:22.04 as ci

ENV LANG C.UTF-8
ARG GITLAB_USER_EMAIL

LABEL maintainer="${GITLAB_USER_EMAIL}"

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y -q \
        lsb-release wget software-properties-common \
        rsync zip unzip bash curl jq
        
RUN bash -c "$(wget -O - https://apt.llvm.org/llvm.sh)"

# Install neuropil build dependencies
RUN DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
        build-essential locate clang clang-tools clang-tidy clang-format-14 git \
        cmake valgrind rustc libffi-dev cargo \
        python3 python3-dev python3-pip python3-venv \
        libxml2-dev libxslt-dev \
        ninja-build libgit2-dev pkg-config automake libtool \
        libncurses5 libncurses5-dev libnanomsg-dev \
        libsodium-dev nanopb graphviz tree retry && \
        apt-get upgrade -y

FROM ci as pre-production

RUN mkdir -p /ci_neuropil/configs
WORKDIR /ci_neuropil

COPY ./do /ci_neuropil/do
COPY ./configs /ci_neuropil/configs
RUN ./do ensure_venv

COPY ./ /ci_neuropil
RUN ./do build --RELEASE all && ./do build --INSTALL
CMD ["bash"]

FROM ubuntu:22.04 as production

ARG GITLAB_USER_EMAIL

LABEL maintainer="${GITLAB_USER_EMAIL}"

COPY --from=pre-production /ci_neuropil/build/neuropil/bin /usr/local/bin
COPY --from=pre-production /ci_neuropil/build/neuropil/lib /usr/local/lib
COPY --from=pre-production /ci_neuropil/include/neuropil* /usr/local/include/

RUN apt-get update && apt-get install -y libsodium23 libncurses5 libncurses5-dev && ldconfig

ENTRYPOINT ["neuropil_node"]

FROM production as python

ARG GITLAB_USER_EMAIL

LABEL maintainer="${GITLAB_USER_EMAIL}"

COPY --from=pre-production "/ci_neuropil/build/bindings/python/dist/neuropil-*.zip" neuropil.zip
RUN apt-get -y install python3-pip && \
    pip3 install neuropil.zip && \
    rm neuropil.zip

ENTRYPOINT ["python3"]
